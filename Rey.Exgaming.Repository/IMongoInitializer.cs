﻿namespace Rey.Exgaming.Repository {
    public interface IMongoInitializer {
        void Initialize();
    }
}
