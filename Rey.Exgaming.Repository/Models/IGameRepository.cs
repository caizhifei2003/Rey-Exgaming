﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using Rey.Exgaming.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Rey.Exgaming.Repository {
    public interface IGameRepository : IModelRepository<Game> {
        void InsertOne(Game model);

        void UpsertOne(Expression<Func<Game, bool>> filter, Game model);

        IFindOneResult<Game> FindOne(string id);

        IFindManyResult<Game> FindMany(string filter, string order, int? size, int? skip);

        IFindManyResult<string> Tags();

        IFindManyResult<string> Platforms();
    }
}
