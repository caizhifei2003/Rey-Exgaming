﻿using MongoDB.Bson;
using MongoDB.Driver;
using Rey.Exgaming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rey.Exgaming.Repository {
    public class GameRepository : ModelRepository<Game>, IGameRepository {
        public GameRepository(IRepository repo)
            : base(repo) {
            var name = this.Name;
        }

        public IFindManyResult<Game> FindMany(string filter, string order, int? size, int? skip) {
            size = size ?? DEFAULT_SIZE;
            var bsonFilter = new BsonDocument();
            var bsonOrder = new BsonDocument();

            if (!string.IsNullOrEmpty(filter)) {
                bsonFilter = BsonDocument.Parse(filter);
            }

            if (!string.IsNullOrEmpty(order)) {
                bsonOrder = BsonDocument.Parse(order);
            }

            var filterResult = this.Collection.Find(bsonFilter);
            var total = filterResult.Count();
            var items = filterResult.Sort(bsonOrder).Skip(skip).Limit(size).ToList();
            return new FindManyResult<Game>(items, total);
        }

        public IFindOneResult<Game> FindOneBySourceId(string id) {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            return new FindOneResult<Game>(this.Collection.Find(x => x.Source.Id == id).FirstOrDefault());
        }

        public IFindManyResult<string> Platforms() {
            var items = this.Collection.Aggregate()
                .Unwind(x => x.Platforms)
                .Group(new BsonDocument("_id", "$platforms.name"))
                .ToEnumerable()
                .Select(x => x["_id"].AsString)
                .ToList();
            return new FindManyResult<string>(items);
        }

        public IFindManyResult<string> Tags() {
            var items = this.Collection.Aggregate()
                .Unwind(x => x.Tags)
                .Group(new BsonDocument("_id", "$tags"))
                .ToEnumerable()
                .Select(x => x["_id"].AsString)
                .ToList();
            return new FindManyResult<string>(items);
        }
    }
}
