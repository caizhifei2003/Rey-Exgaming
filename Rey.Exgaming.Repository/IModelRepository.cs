﻿using Rey.Exgaming.Models;

namespace Rey.Exgaming.Repository {
    public interface IModelRepository<TModel>
        where TModel : class, IModel {
    }
}
