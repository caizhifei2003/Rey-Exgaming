﻿using MongoDB.Bson.Serialization.Conventions;
using Rey.Exgaming.Models;
using Rey.Exgaming.Repository.Conventions;
using System.Linq;

namespace Rey.Exgaming.Repository {
    public class MongoInitializer : IMongoInitializer {
        public IRepositoryOptions Options { get; }

        public MongoInitializer(IRepositoryOptions options) {
            this.Options = options;
        }

        public void Initialize() {
            this.InitIdGeneratorConvention();
            this.InitNamingConvention();
        }

        private void InitIdGeneratorConvention() {
            var name = "Id Generator Convention";
            ConventionRegistry.Register(name,
                    new ConventionPack() { new IdGeneratorConvention() },
                    type => typeof(Model) == type);
        }

        private void InitNamingConvention() {
            var name = "Naming Convention";

            if (this.Options.Naming == NamingConvention.Camel) {
                ConventionRegistry.Register(name,
                    new ConventionPack() { new CamelNamingConvention() },
                    this.Options.NamingFilter);
                return;
            }

            if (this.Options.Naming == NamingConvention.Underscore) {
                ConventionRegistry.Register(name,
                    new ConventionPack() { new UnderscoreNamingConvention() },
                    this.Options.NamingFilter);
                return;
            }
        }
    }
}
