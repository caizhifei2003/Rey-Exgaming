﻿using MongoDB.Bson;
using MongoDB.Driver;
using Rey.Exgaming.Models;
using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Exgaming.Repository {
    public abstract class ModelRepository<TModel> : IModelRepository<TModel>
        where TModel : class, IModel {
        public const int DEFAULT_SIZE = 24;

        protected IRepository Repo { get; }
        protected IRepositoryOptions Options => this.Repo.Options;
        protected IMongoClient Client => this.Repo.Client;
        protected IMongoDatabase Database => this.Client.GetDatabase(this.Options.Database);
        protected IMongoCollection<TModel> Collection => this.Database.GetCollection<TModel>(this.Name);
        protected virtual string Name => Regex.Replace(typeof(TModel).Name, "[A-Z]", (match) => $"{ (match.Index == 0 ? "" : ".") }{ match.Value.ToLower() }");

        public ModelRepository(IRepository repo) {
            this.Repo = repo;
        }

        public virtual void InsertOne(TModel model) {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            this.Collection.InsertOne(model);
        }

        public virtual void ReplaceOne(TModel model) {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (model.Id == null)
                throw new ArgumentNullException(nameof(model.Id));

            this.Collection.ReplaceOne(x => x.Id == model.Id, model);
        }

        public virtual void UpsertOne(Expression<Func<TModel, bool>> filter, TModel model) {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            this.Collection.ReplaceOne(filter, model, new UpdateOptions { IsUpsert = true });
        }

        public IFindOneResult<TModel> FindOne(string id) {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            return new FindOneResult<TModel>(this.Collection.Find(x => x.Id == id).FirstOrDefault());
        }
    }
}
