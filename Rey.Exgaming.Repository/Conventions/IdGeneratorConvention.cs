﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using Rey.Exgaming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rey.Exgaming.Repository.Conventions {
    public class IdGeneratorConvention : IClassMapConvention {
        public string Name => "IdGenerator";

        public void Apply(BsonClassMap classMap) {
            if (typeof(Model) == classMap.ClassType) {
                var member = classMap.ClassType.GetMembers().FirstOrDefault(x => x.Name.Equals("id", StringComparison.CurrentCultureIgnoreCase));
                if (member == null)
                    throw new InvalidOperationException("cannot find id member");

                classMap.MapIdMember(member)
                    .SetIdGenerator(new StringObjectIdGenerator())
                    .SetSerializer(new StringSerializer(BsonType.ObjectId))
                    .SetIgnoreIfDefault(true);
            }
        }
    }
}
