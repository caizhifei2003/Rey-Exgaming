﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using System.Text.RegularExpressions;

namespace Rey.Exgaming.Repository.Conventions {
    public class UnderscoreNamingConvention : IMemberMapConvention {
        public string Name => "UnderscoreNaming";

        public void Apply(BsonMemberMap memberMap) {
            memberMap.SetElementName(Naming(memberMap.MemberName));
        }

        private static string Naming(string name) {
            return Regex.Replace(name, "[A-Z]+", match => {
                return $"{(match.Index > 0 ? "_" : "")}{match.Value.ToLower()}";
            });
        }
    }
}
