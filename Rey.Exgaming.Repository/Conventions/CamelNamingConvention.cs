﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using System.Text.RegularExpressions;

namespace Rey.Exgaming.Repository.Conventions {
    public class CamelNamingConvention : IMemberMapConvention {
        public string Name => "CamelNaming";

        public void Apply(BsonMemberMap memberMap) {
            memberMap.SetElementName(Naming(memberMap.MemberName));
        }

        private static string Naming(string name) {
            return Regex.Replace(name, "^[A-Z]+", match => {
                return match.Value.ToLower();
            });
        }
    }
}
