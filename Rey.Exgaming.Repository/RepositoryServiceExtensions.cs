﻿using Rey.Exgaming.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection {
    public static class RepositoryServiceExtensions {
        public static IServiceCollection AddRepository(this IServiceCollection services, Action<RepositoryOptions> config) {
            var options = new RepositoryOptions();
            config(options);
            services.AddSingleton<IRepositoryOptions>(options);
            services.AddSingleton<IMongoInitializer, MongoInitializer>();

            services.AddSingleton<IRepository, DefaultRepository>();
            services.AddSingleton<IGameRepository, GameRepository>();

            return services;
        }
    }
}
