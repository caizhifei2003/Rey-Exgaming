﻿using System;
using System.Collections.Generic;

namespace Rey.Exgaming.Repository {
    public interface IFindManyResult<TModel> {
        IEnumerable<TModel> Items { get; }
        Int64 Total { get; }
    }

    public interface IFindOneResult<TModel> {
        TModel Item { get; }
    }
}
