﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rey.Exgaming.Repository {
    public class FindManyResult<TModel> : IFindManyResult<TModel> {
        public IEnumerable<TModel> Items { get; }
        public Int64 Total { get; }

        public FindManyResult(IEnumerable<TModel> items, Int64 total) {
            this.Items = items;
            this.Total = total;
        }

        public FindManyResult(IEnumerable<TModel> items)
            : this(items, items.Count()) {
        }
    }

    public class FindOneResult<TModel> : IFindOneResult<TModel> {
        public TModel Item { get; }

        public FindOneResult(TModel item) {
            this.Item = item;
        }
    }
}
