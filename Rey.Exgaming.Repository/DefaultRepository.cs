﻿using MongoDB.Driver;

namespace Rey.Exgaming.Repository {
    public class DefaultRepository : IRepository {
        private IMongoInitializer Initializer { get; }
        public IRepositoryOptions Options { get; }
        public IMongoClient Client { get; }

        public DefaultRepository(IRepositoryOptions options, IMongoInitializer initializer) {
            this.Options = options;
            this.Initializer = initializer;

            this.Initializer.Initialize();

            var settings = new MongoClientSettings() {
                Server = new MongoServerAddress(this.Options.Host, this.Options.Port),
                Credential = MongoCredential.CreateCredential(this.Options.AuthDB, this.Options.User, this.Options.Password),
            };
            this.Client = new MongoClient(settings);
        }
    }
}
