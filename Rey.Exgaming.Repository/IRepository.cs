﻿using MongoDB.Driver;
using System;

namespace Rey.Exgaming.Repository {
    public interface IRepository {
        IRepositoryOptions Options { get; }
        IMongoClient Client { get; }
    }
}
