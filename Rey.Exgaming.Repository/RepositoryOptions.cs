﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rey.Exgaming.Repository {
    public enum NamingConvention {
        Pascal,
        Camel,
        Underscore
    }

    public interface IRepositoryOptions {
        string Host { get; }
        int Port { get; }
        string Database { get; }
        string AuthDB { get; }
        string User { get; }
        string Password { get; }
        NamingConvention Naming { get; }
        Func<Type, bool> NamingFilter { get; }
    }

    public class RepositoryOptions : IRepositoryOptions {
        public string Host { get; set; } = "localhost";
        public int Port { get; set; } = 27017;
        public string Database { get; set; }
        public string AuthDB { get; set; } = "admin";
        public string User { get; set; }
        public string Password { get; set; }
        public NamingConvention Naming { get; set; } = NamingConvention.Camel;
        public Func<Type, bool> NamingFilter { get; set; } = type => type.FullName.StartsWith("Rey.Exgaming.Models");
    }
}
