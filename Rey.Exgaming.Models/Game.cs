﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rey.Exgaming.Models {
    public class GameSource {
        public string Id { get; set; }
        public string Name { get; set; }

        public GameSource(string id, string name) {
            this.Id = id;
            this.Name = name;
        }
    }

    public class GamePrice {
        public int? Original { get; set; }
        public int? Final { get; set; }

        public GamePrice(int? original = null, int? final = null) {
            this.Original = original;
            this.Final = final;
        }
    }

    public class GamePlatform {
        public string Name { get; set; }
        public string Minimum { get; set; }
        public string Recommended { get; set; }

        public GamePlatform(string name, string minimum = null, string recommended = null) {
            this.Name = name;
            this.Minimum = minimum;
            this.Recommended = recommended;
        }
    }

    public class GameRating {
        public double Max { get; set; }
        public double Min { get; set; }
        public double Average { get; set; }
        public int Raters { get; set; }
    }

    public class Game : Model {
        public GameSource Source { get; set; }
        public string Name { get; set; }
        public bool IsFree { get; set; }
        public string CoverUrl { get; set; }
        public string Description { get; set; }
        public DateTime? PublishDate { get; set; }
        public List<string> Developers { get; set; } = new List<string>();
        public List<string> Publishers { get; set; } = new List<string>();
        public List<string> Languages { get; set; } = new List<string>();
        public List<Picture> Pictures { get; set; } = new List<Picture>();
        public List<Link> Links { get; set; } = new List<Link>();
        public GamePrice Price { get; set; }
        public List<GamePlatform> Platforms { get; set; } = new List<GamePlatform>();
        public List<string> Tags { get; set; } = new List<string>();

        public GameRating Rating { get; set; }
    }
}
