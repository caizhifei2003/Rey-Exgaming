﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rey.Exgaming.Models {
    public class Medium {
        public string Url { get; set; }
    }

    public class Picture : Medium {
        public string Thumbnail { get; set; }
    }

    public class Video : Medium {
        public string Cover { get; set; }
    }
}
