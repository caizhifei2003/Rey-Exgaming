﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rey.Exgaming.Models {
    public class Date {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public Date(int year, int month, int day) {
            this.Year = year;
            this.Month = month;
            this.Day = day;
        }

        public Date(DateTime dateTime) {
            this.Year = dateTime.Year;
            this.Month = dateTime.Month;
            this.Day = dateTime.Day;
        }

        public Date() {
        }

        public DateTime ToDateTime() {
            return new DateTime(this.Year, this.Month, this.Day);
        }

        public override string ToString() {
            return this.ToDateTime().ToShortDateString();
        }

        public virtual string ToString(string format) {
            return this.ToDateTime().ToString(format);
        }

        public static Date Parse(string content) {
            if (string.IsNullOrEmpty(content))
                return null;

            return DateTime.Parse(content);
        }

        public static Date TryParse(string content) {
            try {
                return Parse(content);
            } catch (FormatException) {
                return null;
            }
        }

        public static implicit operator Date(DateTime dateTime) {
            return new Date(dateTime);
        }

        public static implicit operator Date(string content) {
            if (string.IsNullOrEmpty(content))
                return null;

            return DateTime.Parse(content);
        }
    }
}
