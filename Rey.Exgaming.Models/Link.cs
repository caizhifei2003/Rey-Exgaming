﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rey.Exgaming.Models {
    public class Link {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
