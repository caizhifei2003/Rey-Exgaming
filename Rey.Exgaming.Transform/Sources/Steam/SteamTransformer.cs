﻿using MongoDB.Bson;
using MongoDB.Driver;
using Rey.Exgaming.Models;
using Rey.Exgaming.Repository;
using Rey.Exgaming.Transform.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Rey.Exgaming.Transform.Sources.Steam {
    public class SteamTransformer : Transformer {
        private ILogger Logger { get; }
        private IMongoCollection<BsonDocument> CollData => this.Rep.Client.GetDatabase("exgaming").GetCollection<BsonDocument>("capture.steam.data");
        private IGameRepository RepGame { get; }

        public SteamTransformer(IRepository rep, IGameRepository repGame, ILogger logger)
            : base(rep) {
            this.Logger = logger;
            this.RepGame = repGame;
        }

        public override void Start() {
            var content = File.ReadAllText("./Config/steam_filter.json");
            var filter = BsonDocument.Parse(content);

            var docs = this.CollData.Find(filter).ToList();
            foreach (var doc in docs) {
                var id = doc["_id"].AsInt32.ToString();
                var model = this.Map(id, doc);
                if (model != null) {
                    this.RepGame.UpsertOne(x => x.Source.Id == id, model);
                    this.Logger.Log($"{model.Source.Name}:{model.Source.Id}:{model.Name}", LogAction.Insert);
                }
            }
        }

        public static List<string> InvalidTags { get; } = new List<string>() { "财务管理", "照片编辑", "教育", "设计和插画", "音频制作", "使用工具", "视频制作", "动画制作和建模", "软件培训" };
        private Game Map(string id, BsonDocument doc) {
            var model = new Game();

            model.Source = new GameSource(id, "steam");
            model.Name = doc["name"].AsString;
            model.IsFree = doc["is_free"].AsBoolean;
            model.CoverUrl = doc["header_image"].AsString;
            model.Description = doc["about_the_game"].AsString;
            model.PublishDate = Date.TryParse(doc["release_date"]["date"].AsString)?.ToDateTime();

            if (doc.Contains("developers")) {
                doc["developers"].AsBsonArray.ToList().ForEach(item => model.Developers.Add(item.AsString));
            }

            if (doc.Contains("publishers")) {
                doc["publishers"].AsBsonArray.ToList().ForEach(item => model.Publishers.Add(item.AsString));
            }

            if (doc.Contains("supported_languages")) {
                doc["supported_languages"].AsString.Split(",").ToList().ForEach(item => {
                    model.Languages.Add(Regex.Replace(item.Trim(), "(\\<(.*?)\\>)|(\\*)", ""));
                });
            }

            if (doc.Contains("screenshots")) {
                doc["screenshots"].AsBsonArray.ToList()
                    .ForEach(item => model.Pictures.Add(new Picture { Url = item["path_full"].AsString, Thumbnail = item["path_thumbnail"].AsString }));
            }
            model.Links.Add(new Link { Name = "steam", Url = $"http://store.steampowered.com/app/{id}" });

            if (!model.IsFree) {
                var price = doc["price_overview"];
                model.Price = new GamePrice(price["initial"].AsInt32, price["final"].AsInt32);
            }

            foreach (var pair in new Dictionary<string, string> {
                { "windows", "pc_requirements" },
                { "mac", "mac_requirements" },
                { "linux", "linux_requirements" }
            }) {
                if (doc["platforms"][pair.Key].AsBoolean) {
                    var platform = new GamePlatform(pair.Key);
                    var req = doc[pair.Value] as BsonDocument;
                    if (req != null) {
                        if (req.Contains("minimum")) {
                            platform.Minimum = Regex.Replace(req["minimum"].AsString.Trim(), "(\\<(.*?)\\>)", "");
                        }

                        if (req.Contains("recommended")) {
                            platform.Recommended = Regex.Replace(req["recommended"].AsString.Trim(), "(\\<(.*?)\\>)", "");
                        }
                    }
                    model.Platforms.Add(platform);
                }
            }

            if (doc.Contains("genres")) {
                doc["genres"].AsBsonArray.ToList().ForEach(item => model.Tags.Add(item["description"].AsString.Trim()));
                if (model.Tags.Any(x => InvalidTags.Contains(x))) {
                    return null;
                }
            }

            return model;
        }
    }
}
