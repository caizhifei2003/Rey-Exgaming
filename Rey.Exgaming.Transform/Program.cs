﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Rey.Exgaming.Transform.Core;
using System.Linq;

namespace Rey.Exgaming.Transform {
    class Program {
        static void Main(string[] args) {
            var services = new ServiceCollection();
            services.AddRepository(options => {
                options.Database = "exgaming";
                options.User = "admin";
                options.Password = "admin123~";
            });

            services.AddTransform(args);

            var provider = services.BuildServiceProvider();

            provider.GetServices<ITransformer>().ToList()
                .ForEach(item => item.Start());
        }
    }
}
