﻿using Rey.Exgaming.Transform.Core;
using Rey.Exgaming.Transform.Sources.Steam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection {
    public static class TransformServiceExtensions {
        public static IServiceCollection AddTransform(this IServiceCollection services, string[] args) {
            services.AddSingleton<ILogger, Logger>();

            if (HasArg(args, "all") || HasArg(args, "steam")) {
                services.AddSingleton<ITransformer, SteamTransformer>();
            }

            //if (HasArg(args, "all") || HasArg(args, "igdb")) {
            //    services.AddSingleton<ITransformer, IGDBTransformer>();
            //}

            return services;
        }

        private static bool HasArg(string[] args, string value) {
            return args.Any(item => item.Equals(value, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
