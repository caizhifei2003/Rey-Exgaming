﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rey.Exgaming.Transform.Core {
    public class Logger : ILogger {
        public ILogger Log(string content) {
            Console.WriteLine(content);
            return this;
        }

        public ILogger Log(string content, LogAction action) {
            return ConsoleLogColor(() => this.Log($"[{action}][{DateTime.Now}]{content}"), GetColor(action));
        }

        public ILogger LogSameLine(string content) {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(content);
            return this;
        }

        public ILogger LogSameLine(string content, LogAction action) {
            return ConsoleLogColor(() => this.LogSameLine($"[{action}][{DateTime.Now}]{content}"), GetColor(action));
        }

        private static ILogger ConsoleLogColor(Func<ILogger> action, ConsoleColor color) {
            var old = Console.ForegroundColor;
            Console.ForegroundColor = color;
            var logger = action();
            Console.ForegroundColor = old;
            return logger;
        }

        private static ConsoleColor GetColor(LogAction action) {
            switch (action) {
                case LogAction.Insert:
                    return ConsoleColor.Green;
                case LogAction.Update:
                    return ConsoleColor.Yellow;
                case LogAction.Delete:
                    return ConsoleColor.Red;
                case LogAction.Skip:
                    return ConsoleColor.DarkYellow;
                case LogAction.Success:
                    return ConsoleColor.DarkGreen;
                case LogAction.Fail:
                    return ConsoleColor.DarkRed;
                default:
                    return Console.ForegroundColor;
            }
        }
    }
}
