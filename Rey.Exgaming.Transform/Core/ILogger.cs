﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rey.Exgaming.Transform.Core {
    public enum LogAction {
        Insert,
        Update,
        Delete,
        Skip,
        Success,
        Fail
    }

    public interface ILogger {
        ILogger Log(string content);
        ILogger Log(string content, LogAction action);
        ILogger LogSameLine(string content);
        ILogger LogSameLine(string content, LogAction action);
    }
}
