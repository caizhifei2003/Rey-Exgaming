﻿using Rey.Exgaming.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rey.Exgaming.Transform.Core {
    public interface ITransformer {
        void Start();
    }

    public abstract class Transformer : ITransformer {
        protected IRepository Rep { get; }

        public Transformer(IRepository rep) {
            this.Rep = rep;
        }

        public abstract void Start();
    }
}
