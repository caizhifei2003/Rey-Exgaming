﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rey.Exgaming.Transform {
    public static class TaskExtensions {
        public static TResult WaitResult<TResult>(this Task<TResult> task) {
            task.Wait();
            return task.Result;
        }
    }
}
