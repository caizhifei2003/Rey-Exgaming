﻿using Microsoft.AspNetCore.Mvc;
using Rey.Exgaming.API.Discovery.Attributes;
using Rey.Exgaming.API.Services.Api;
using Rey.Exgaming.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rey.Exgaming.API.Api {
    [Route("api/[controller]")]
    public class TagController : Controller {
        private IApiContext ApiContext { get; }
        private IGameRepository RepoGame { get; }

        public TagController(IApiContext apiContext, IGameRepository repoGame) {
            this.ApiContext = apiContext;
            this.RepoGame = repoGame;
        }

        [HttpGet]
        [ApiMethod("/api/tag")]
        public IActionResult GetTags() {
            return this.ApiContext.Handle(() => {
                return this.RepoGame.Tags();
            });
        }
    }
}
