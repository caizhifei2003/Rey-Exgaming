﻿using Microsoft.AspNetCore.Mvc;
using Rey.Exgaming.API.Discovery.Attributes;
using Rey.Exgaming.API.Exceptions;
using Rey.Exgaming.API.Services.Api;
using Rey.Exgaming.Repository;
using System;
using System.Text;

namespace Rey.Exgaming.API.Api {
    [Route("api/[controller]")]
    public class GameController : Controller {
        private IApiContext ApiContext { get; }
        private IGameRepository RepoGame { get; }

        public GameController(IApiContext apiContext, IGameRepository repoGame) {
            this.ApiContext = apiContext;
            this.RepoGame = repoGame;
        }

        [HttpGet("{id}")]
        [ApiMethod("/api/game{/id}")]
        public IActionResult GetGame([ApiParameter(true)]string id) {
            return this.ApiContext.Handle(() => {
                if (string.IsNullOrEmpty(id))
                    throw ApiException.Query.EmptyId("id is empty");

                try {
                    return this.RepoGame.FindOne(id);
                } catch (FormatException) {
                    throw ApiException.Query.InvalidId("invalid id format");
                }
            });
        }

        [HttpGet]
        [ApiMethod("/api/game")]
        public IActionResult GetGames([ApiParameter]string filter
            , [ApiParameter]string order
            , [ApiParameter]int? skip
            , [ApiParameter]int? size = 24
            ) {
            return this.ApiContext.Handle(() => {
                return this.RepoGame.FindMany(
                    this.ApiContext.DecodeFilter(filter),
                    this.ApiContext.DecodeOrder(order),
                    size,
                    skip);
            });
        }
    }
}
