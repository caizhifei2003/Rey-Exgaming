﻿using System;

namespace Rey.Exgaming.API.Exceptions {
    public class ApiExceptionGenerator {
        public Int64 BeginCode { get; }
        public ApiExceptionGenerator(Int64 beginCode) {
            this.BeginCode = beginCode;
        }

        public virtual ApiException Generate(Int64 code, string message) {
            return new ApiException(new ApiError(this.BeginCode + code, message));
        }
    }
}
