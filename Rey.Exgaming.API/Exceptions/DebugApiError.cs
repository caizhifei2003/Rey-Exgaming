﻿using System;

namespace Rey.Exgaming.API.Exceptions {
    public class DebugApiError : ApiError {
        public string Stack { get; }
        public string Type { get; }

        public DebugApiError(Exception ex)
            : base(-1L, ex.Message) {
            this.Stack = ex.StackTrace;
            this.Type = ex.GetType().FullName;
        }
    }
}
