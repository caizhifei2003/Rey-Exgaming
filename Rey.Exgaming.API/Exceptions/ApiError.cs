﻿using System;

namespace Rey.Exgaming.API.Exceptions {
    public class ApiError {
        public Int64 Code { get; }
        public string Message { get; }

        public ApiError(Int64 code, string message) {
            this.Code = code;
            this.Message = message;
        }

        public static ApiError Unknown { get; } = new ApiError(-1L, "unknown api error");
        public static ApiError Success { get; } = new ApiError(0L, "success");
    }
}
