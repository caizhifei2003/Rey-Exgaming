﻿namespace Rey.Exgaming.API.Exceptions.Generators {
    public class AuthApiExceptionGenerator : ApiExceptionGenerator {
        public AuthApiExceptionGenerator(long beginCode)
            : base(beginCode) {
        }

        public ApiException InvalidUser(string message) => Generate(0x01L, message);
        public ApiException InvalidPassword(string message) => Generate(0x02L, message);
        public ApiException InvalidUserOrPassword(string message) => Generate(0x03L, message);
        public ApiException InvalidEmail(string message) => Generate(0x04L, message);
        public ApiException InvalidMobile(string message) => Generate(0x05L, message);
    }
}
