﻿namespace Rey.Exgaming.API.Exceptions.Generators {
    public class QueryApiExceptionGenerator : ApiExceptionGenerator {
        public QueryApiExceptionGenerator(long beginCode)
            : base(beginCode) {
        }

        public ApiException EmptyId(string message) => Generate(0x01L, message);
        public ApiException InvalidId(string message) => Generate(0x02L, message);
    }
}
