﻿using Rey.Exgaming.API.Exceptions.Generators;
using System;

namespace Rey.Exgaming.API.Exceptions {
    public class ApiException : Exception {
        public ApiError Error { get; }
        public ApiException(ApiError error)
            : base($"{error.Code}: {error.Message}") {
            this.Error = error;
        }

        public static ApiException Unknown { get; } = new ApiException(ApiError.Unknown);

        public static AuthApiExceptionGenerator Auth { get; } = new AuthApiExceptionGenerator(0x0000);
        public static QueryApiExceptionGenerator Query { get; } = new QueryApiExceptionGenerator(0x0100);
    }
}
