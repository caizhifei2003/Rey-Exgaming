﻿using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Rey.Exgaming.API.Discovery.Attributes {
    [AttributeUsage(AttributeTargets.Method)]
    public class ApiMethodAttribute : Attribute {
        public string Path { get; }

        public ApiMethodAttribute(string path) {
            this.Path = path;
        }

        public object GetApiInfo(MethodInfo method) {
            dynamic info = new ExpandoObject();
            info.name = method.Name;
            info.path = this.Path;
            info.method = method.GetCustomAttributes<HttpMethodAttribute>().ToList().SelectMany(x => x.HttpMethods).FirstOrDefault() ?? "GET";
            info.parameters = method.GetParameters().Select(x => x.GetCustomAttribute<ApiParameterAttribute>().GetApiInfo(x)).Where(x => x != null);
            return info;
        }
    }
}
