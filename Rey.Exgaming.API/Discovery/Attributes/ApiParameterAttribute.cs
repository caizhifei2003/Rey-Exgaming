﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Rey.Exgaming.API.Discovery.Attributes {
    [AttributeUsage(AttributeTargets.Parameter)]
    public class ApiParameterAttribute : Attribute {
        public bool Required { get; }
        public object Default { get; }

        public ApiParameterAttribute(bool required = false, object def = null) {
            this.Required = required;
            this.Default = def;
        }

        public object GetApiInfo(ParameterInfo parameter) {
            dynamic info = new ExpandoObject();
            info.name = parameter.Name;
            info.required = this.Required;
            if (parameter.HasDefaultValue) {
                info.@default = parameter.DefaultValue;
            }

            return info;
        }
    }
}
