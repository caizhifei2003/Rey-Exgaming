﻿using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Rey.Exgaming.API.Services.Jwt;

namespace Microsoft.Extensions.DependencyInjection {
    public static class JwtServiceCollectionExtensions {
        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services, Action<JwtOptions> configure) {
            var jwtOptions = new JwtOptions();
            configure(jwtOptions);

            services.AddSingleton(jwtOptions);

            services.AddAuthentication(options => {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options => {
                options.TokenValidationParameters = jwtOptions.GetParameters();
            });
            return services;
        }
    }
}
