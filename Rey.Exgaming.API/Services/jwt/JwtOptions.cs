﻿using System;
using System.Collections.Generic;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Cryptography;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Rey.Exgaming.API.Services.Jwt {
    public class JwtOptions {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public double Expires { get; set; }
        public string Key { get; set; }
        public JwtValidate Validate { get; set; }

        public SecurityKey GetKey() {
            return new SymmetricSecurityKey(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(this.Key)));
        }

        public TokenValidationParameters GetParameters() {
            return new TokenValidationParameters {
                ValidateIssuer = this.Validate.Issuer,
                ValidateAudience = this.Validate.Audience,
                ValidateLifetime = this.Validate.Lifetime,
                ValidateIssuerSigningKey = this.Validate.Key,

                ValidIssuer = this.Issuer,
                ValidAudience = this.Audience,
                IssuerSigningKey = this.GetKey()
            };
        }

        public JwtSecurityToken GetToken(IEnumerable<Claim> claims) {
            return new JwtSecurityToken(
                issuer: this.Issuer,
                audience: this.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(this.Expires),
                signingCredentials: new SigningCredentials(this.GetKey(), SecurityAlgorithms.HmacSha256));
        }

        public string GetTokenString(IEnumerable<Claim> claims) {
            return new JwtSecurityTokenHandler().WriteToken(this.GetToken(claims));
        }

        public class JwtValidate {
            public bool Issuer { get; set; }
            public bool Audience { get; set; }
            public bool Lifetime { get; set; }
            public bool Key { get; set; }
        }
    }
}
