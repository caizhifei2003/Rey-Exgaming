﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Rey.Exgaming.API.Services.Cors;

namespace Microsoft.AspNetCore.Builder {
    public static class ConfigurableCorsAppExtensions {
        public static IApplicationBuilder UseConfigurableCors(this IApplicationBuilder app) {
            var options = app.ApplicationServices.GetService<ConfigurableCorsOptions>();
            if (options == null)
                return app;

            app.UseCors(options.Policy);
            return app;
        }
    }
}
