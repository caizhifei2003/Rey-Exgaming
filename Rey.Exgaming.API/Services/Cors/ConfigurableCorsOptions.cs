﻿using System.Collections.Generic;

namespace Rey.Exgaming.API.Services.Cors {
    public class ConfigurableCorsOptions {
        public string Policy { get; set; }
        public List<ConfigurableCorsPolicy> Policies { get; } = new List<ConfigurableCorsPolicy>();
    }
}
