﻿using System.Linq;
using System;
using Rey.Exgaming.API.Services.Cors;

namespace Microsoft.Extensions.DependencyInjection {
    public static class ConfigurableCorsServiceCollectionExtensions {
        public static IServiceCollection AddConfigurableCors(this IServiceCollection services, Action<ConfigurableCorsOptions> configure) {
            var options = new ConfigurableCorsOptions();
            configure(options);

            if (string.IsNullOrEmpty(options.Policy)) {
                return services;
            }

            if (!options.Policies.Any(item => item.Name.Equals(options.Policy)))
                throw new InvalidOperationException($"cannot find policy: {options.Policy}");

            services.AddSingleton(options);
            services.AddCors(op => {
                options.Policies.ForEach(item => op.AddPolicy(item.Name, item.ToPolicy()));
            });
            return services;
        }
    }
}
