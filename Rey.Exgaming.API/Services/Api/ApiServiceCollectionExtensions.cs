﻿using Rey.Exgaming.API.Services.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.Extensions.DependencyInjection {
    public static class ApiServiceCollectionExtensions {
        public static IServiceCollection AddApi(this IServiceCollection services, Action<ApiOptions> configure) {
            var options = new ApiOptions();
            configure(options);

            services.AddSingleton<IApiContext, ApiContext>();
            return services;
        }
    }
}
