﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Rey.Exgaming.API.Exceptions;
using System;
using System.Dynamic;
using System.Text;

namespace Rey.Exgaming.API.Services.Api {
    public class ApiContext : IApiContext {
        private IHostingEnvironment Env { get; }

        public ApiContext(IHostingEnvironment env) {
            this.Env = env;
        }

        public IActionResult Handle(Action action) {
            dynamic data = new ExpandoObject();
            data.error = ApiError.Success;
            try {
                action();
            } catch (ApiException ex) {
                data.error = ex.Error;
            } catch (Exception ex) {
                if (this.Env.IsDevelopment()) {
                    data.error = new DebugApiError(ex);
                } else {
                    data.error = ApiError.Unknown;
                }
            }
            return new JsonResult(data);
        }

        public IActionResult Handle<TResult>(Func<TResult> func) {
            dynamic data = new ExpandoObject();
            data.error = ApiError.Success;
            try {
                data.result = func();
            } catch (ApiException ex) {
                data.error = ex.Error;
            } catch (Exception ex) {
                if (this.Env.IsDevelopment()) {
                    data.error = new DebugApiError(ex);
                } else {
                    data.error = ApiError.Unknown;
                }
            }
            return new JsonResult(data);
        }

        public string DecodeFilter(string filter) {
            if (string.IsNullOrEmpty(filter))
                return filter;

            return Encoding.UTF8.GetString(Convert.FromBase64String(filter));
        }

        public string DecodeOrder(string order) {
            if (string.IsNullOrEmpty(order))
                return order;

            return Encoding.UTF8.GetString(Convert.FromBase64String(order));
        }
    }
}
