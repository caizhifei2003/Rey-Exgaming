﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace Rey.Exgaming.API.Services.Api {
    public interface IApiContext {
        IActionResult Handle(Action action);
        IActionResult Handle<TResult>(Func<TResult> func);
        string DecodeFilter(string filter);
        string DecodeOrder(string order);
    }
}
