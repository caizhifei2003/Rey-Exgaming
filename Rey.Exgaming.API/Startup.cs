﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Rey.Exgaming.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Rey.Exgaming.API {

    public class Startup {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddConfigurableCors(options => {
                this.Configuration.GetSection("cors").Bind(options);
            });

            services.AddRepository(options => {
                this.Configuration.GetSection("repo").Bind(options);
            });

            services.AddJwtAuthentication(options => {
                this.Configuration.GetSection("jwt").Bind(options);
            });

            services.AddApi(options => {
                this.Configuration.GetSection("api").Bind(options);
            });

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseConfigurableCors();

            app.UseAuthentication();
            app.UseMvcWithDefaultRoute();
        }
    }
}
