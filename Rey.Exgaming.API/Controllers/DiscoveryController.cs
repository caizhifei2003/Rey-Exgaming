﻿using Microsoft.AspNetCore.Mvc;
using Rey.Exgaming.API.Discovery.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Rey.Exgaming.API.Controllers {
    public class DiscoveryController : Controller {
        public IActionResult Index() {
            var ass = Assembly.GetExecutingAssembly();
            var methods = ass.GetTypes().SelectMany(type => type.GetMethods())
                .Select(method => method.GetCustomAttribute<ApiMethodAttribute>()?.GetApiInfo(method))
                .Where(method => method != null);

            return Json(methods);
        }
    }
}
