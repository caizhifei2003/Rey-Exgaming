﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Rey.Exgaming.API.Controllers {
    public class HomeController : Controller {
        public IActionResult Create() {
            var claims = new List<Claim>(){
                new Claim("id", "123456789"),
                new Claim("name","kevin cai")
            };

            var credential = new SigningCredentials(new SymmetricSecurityKey(Convert.FromBase64String("12345678901234567890123456789012")), SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken("exgaming api", "exgaming web", claims, null, DateTime.Now.AddMinutes(10), credential);

            var handler = new JwtSecurityTokenHandler();
            return Content(handler.WriteToken(token));
        }

        public IActionResult Validate(string token) {
            var parameters = new TokenValidationParameters() {
                IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String("12345678901234567890123456789012")),
                ValidateIssuer = true,
                ValidIssuer = "exgaming api",
                ValidateAudience = true,
                ValidAudiences = new string[] { "exgaming web" },
                ValidateLifetime = true
            };

            var handler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken = null;
            var claims = handler.ValidateToken(token, parameters, out validatedToken);

            var jwtToken = validatedToken as JwtSecurityToken;
            return Json(new {
                id = jwtToken.Claims.First(x => x.Type == "id").Value,
                name = jwtToken.Claims.First(x => x.Type == "name").Value
            });
        }
    }
}
