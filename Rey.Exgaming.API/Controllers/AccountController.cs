﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Exgaming.API.Services.Jwt;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Rey.Exgaming.API.Controllers {
    public class AccountController : Controller {
        private JwtOptions JwtOptions { get; }

        public AccountController(JwtOptions jwtOptions) {
            this.JwtOptions = jwtOptions;
        }

        public IActionResult Login() {
            var claims = new List<Claim> {
              new Claim(JwtRegisteredClaimNames.Sub, "123"),
              new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            return Content(this.JwtOptions.GetTokenString(claims));
        }

        [Authorize]
        public IActionResult Test() {
            return Content("Test");
        }

        public IActionResult Test2() {
            return Content("Test2");
        }
    }
}
