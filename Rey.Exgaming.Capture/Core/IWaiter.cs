﻿namespace Rey.Exgaming.Capture {
    public interface IWaiter {
        void Wait(double seconds);
    }
}
