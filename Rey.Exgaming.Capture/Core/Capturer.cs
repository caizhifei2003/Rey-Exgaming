﻿using Rey.Exgaming.Repository;

namespace Rey.Exgaming.Capture {
    public abstract class Capturer : ICapturer {
        protected IRepository Rep { get; }

        public Capturer(IRepository rep) {
            this.Rep = rep;
        }

        public abstract void Start();
    }
}
