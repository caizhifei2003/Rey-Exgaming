﻿using System;
using System.Threading;

namespace Rey.Exgaming.Capture {
    public class Waiter : IWaiter {
        private ILogger Logger { get; }

        public Waiter(ILogger logger) {
            this.Logger = logger;
        }

        public void Wait(double seconds) {
            var begin = DateTime.Now;
            this.Logger.Log("start waiting...");
            while (true) {
                Thread.Sleep(1000);

                var current = (DateTime.Now - begin).TotalSeconds;
                if (current > seconds)
                    break;

                Console.SetCursorPosition(0, Console.CursorTop);
                this.Logger.LogSameLine($"waiting: {Math.Floor(current)}/{seconds}");
            }
            this.Logger.Log("end waiting");
        }
    }
}
