﻿using Rey.Exgaming.Capture;
using Rey.Exgaming.Capture.Sources.IGDB;
using Rey.Exgaming.Capture.Sources.Steam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection {
    public static class CaptureServiceExtensions {
        public static IServiceCollection AddCapture(this IServiceCollection services, string[] args) {
            services.AddSingleton<ILogger, Logger>();
            services.AddSingleton<IWaiter, Waiter>();

            if (HasArg(args, "all") || HasArg(args, "steam") || HasArg(args, "steam:id")) {
                services.AddSingleton<ICapturer, SteamIdCapturer>();
            }

            if (HasArg(args, "all") || HasArg(args, "steam") || HasArg(args, "steam:data")) {
                services.AddSingleton<ICapturer, SteamDataCapturer>();
            }

            if (HasArg(args, "all") || HasArg(args, "igdb") || HasArg(args, "igdb:id") || HasArg(args, "igdb:data")) {
                services.AddSingleton<IIGDBCredentialManager, IGDBCredentialManager>();
            }

            if (HasArg(args, "all") || HasArg(args, "igdb") || HasArg(args, "igdb:id")) {
                services.AddSingleton<ICapturer, IGDBIdCapturer>();
            }

            if (HasArg(args, "all") || HasArg(args, "igdb") || HasArg(args, "igdb:data")) {
                services.AddSingleton<ICapturer, IGDBDataCapturer>();
            }

            return services;
        }

        private static bool HasArg(string[] args, string value) {
            return args.Any(item => item.Equals(value, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
