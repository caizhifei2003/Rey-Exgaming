﻿using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rey.Exgaming.Repository;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Rey.Exgaming.Capture.Sources.Steam {
    public class SteamDataCapturer : Capturer {
        private IWaiter Waiter { get; }
        private ILogger Logger { get; }
        private IMongoCollection<BsonDocument> CollId => this.Rep.Client.GetDatabase("exgaming").GetCollection<BsonDocument>("capture.steam.id");
        private IMongoCollection<BsonDocument> CollData => this.Rep.Client.GetDatabase("exgaming").GetCollection<BsonDocument>("capture.steam.data");

        public SteamDataCapturer(IRepository rep, IWaiter waiter, ILogger logger)
            : base(rep) {
            this.Waiter = waiter;
            this.Logger = logger;
        }

        public override void Start() {
            this.CollId.Find(new BsonDocument())
                 .ToList()
                 .ForEach(item => {
                     this.CaptureData(item["_id"].AsInt32, item["name"].AsString);
                 });
        }

        private void CaptureData(int id, string name) {
            if (this.CollData.Find(new BsonDocument("_id", id)).Count() > 0) {
                this.Logger.Log($"{id}:{name}", LogAction.Skip);
                return;
            }

            using (var client = new HttpClient()) {
                while (true) {
                    var request = new HttpRequestMessage(HttpMethod.Get, $"http://store.steampowered.com/api/appdetails?appids={id}&cc=CNY");
                    request.Headers.Add("Accept-Language", "zh-CN,zh;q=0.9,ar;q=0.8,he;q=0.7,en;q=0.6,en-US;q=0.5,no;q=0.4,nb;q=0.3,nn;q=0.2,ja;q=0.1,zh-TW;q=0.1");

                    var content = client.SendAsync(request).GetAwaiter().GetResult()
                        .Content.ReadAsStringAsync().GetAwaiter().GetResult();

                    try {
                        var body = JsonConvert.DeserializeObject<JToken>(content).Value<JToken>(id.ToString());
                        var success = body.Value<bool>("success");
                        if (!success) {
                            this.CollData.InsertOne(new BsonDocument("_id", id));
                            this.Logger.Log($"{id}:{name}", LogAction.Fail);
                            break;
                        }

                        var data = body.Value<JToken>("data") as JObject;
                        var isFree = data.Value<bool>("is_free");
                        if (!isFree) {
                            var currency = data.Value<JToken>("price_overview")?.Value<string>("currency");
                            if (currency != null && !currency.Equals("CNY", StringComparison.CurrentCultureIgnoreCase)) {
                                throw new InvalidOperationException("Invalid currency");
                            }
                        }

                        data.Add("_id", id);
                        var model = BsonDocument.Parse(JsonConvert.SerializeObject(data));
                        this.CollData.InsertOne(model);
                        this.Logger.Log($"{id}:{name}", LogAction.Success);
                        break;
                    } catch (JsonReaderException) {
                        this.Waiter.Wait(100);
                    }
                }
            }
        }
    }
}
