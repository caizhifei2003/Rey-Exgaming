﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rey.Exgaming.Repository;

namespace Rey.Exgaming.Capture.Sources.Steam {
    public class SteamIdCapturer : Capturer, ICapturer {
        private IWaiter Waiter { get; }
        private ILogger Logger { get; }
        private IMongoCollection<BsonDocument> CollId => this.Rep.Client.GetDatabase("exgaming").GetCollection<BsonDocument>("capture.steam.id");

        public SteamIdCapturer(IRepository rep, IWaiter waiter, ILogger logger)
            : base(rep) {
            this.Waiter = waiter;
            this.Logger = logger;
        }

        public override void Start() {
            using (var client = new HttpClient()) {
                var content = client.GetAsync("https://api.steampowered.com/ISteamApps/GetAppList/v2/").GetAwaiter().GetResult()
                    .Content.ReadAsStringAsync().GetAwaiter().GetResult();

                var list = JsonConvert.DeserializeObject<JToken>(content)
                    .Value<JToken>("applist")
                    .Value<JArray>("apps")
                    .Select(item => (item.Value<int>("appid"), item.Value<string>("name")));

                foreach (var (appid, name) in list) {
                    var filter = Builders<BsonDocument>.Filter.Eq("_id", appid);
                    var model = this.CollId.Find(filter).FirstOrDefault();
                    if (model != null) {
                        if (model["name"].AsString != name) {
                            this.CollId.UpdateOne(filter, Builders<BsonDocument>.Update.Set("name", name));
                        }
                        continue;
                    }

                    model = new BsonDocument {
                        { "_id", appid },
                        { "name", name }
                    };
                    CollId.InsertOne(model);
                }
            }
        }
    }
}
