﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rey.Exgaming.Repository;

namespace Rey.Exgaming.Capture.Sources.IGDB {
    public class IGDBDataCapturer : Capturer {
        private IIGDBCredentialManager Credential { get; }
        private ILogger Logger { get; }
        private IMongoCollection<BsonDocument> CollId => this.Rep.Client.GetDatabase("exgaming").GetCollection<BsonDocument>("capture.igdb.id");
        private IMongoCollection<BsonDocument> CollData => this.Rep.Client.GetDatabase("exgaming").GetCollection<BsonDocument>("capture.igdb.data");

        public IGDBDataCapturer(IRepository rep, IIGDBCredentialManager credential, ILogger logger)
            : base(rep) {
            this.Credential = credential;
            this.Logger = logger;
        }

        public override void Start() {
            var list = this.CollId.Find(new BsonDocument())
                .ToEnumerable()
                .Select(item => item["_id"].AsInt32)
                .ToList();

            var count = 0;
            while (count < list.Count) {
                var group = list.Skip(count).Take(1000).ToList();
                this.CaptureData(group);
                count += group.Count;
            }
        }

        private void CaptureData(IEnumerable<int> group) {
            var list = group.ToList();
            var inserts = list.Where(id => this.CollData.Find(new BsonDocument("_id", id)).Count() == 0).ToList();

            var skipped = list.Count - inserts.Count;
            if (skipped > 0) {
                this.Logger.Log($"{skipped}", LogAction.Skip);
            }

            if (inserts.Count == 0)
                return;

            using (var client = new HttpClient()) {
                while (true) {
                    var credential = this.Credential.Get();
                    if (credential == null) {
                        throw new InvalidOperationException("cannot get credential for igdb");
                    }

                    var request = new HttpRequestMessage(HttpMethod.Get, $"{credential.Url}/games/{string.Join(",", inserts)}");
                    request.Headers.Add("user-key", credential.Key);
                    request.Headers.Add("Accept", "application/json");

                    var resp = client.SendAsync(request).GetAwaiter().GetResult();
                    if (resp.StatusCode == System.Net.HttpStatusCode.Forbidden) {
                        this.Logger.Log($"credential forbidden. {credential.Key}", LogAction.Skip);
                        this.Credential.Fail(credential.Key);
                        continue;
                    }

                    var content = resp.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    var models = JsonConvert.DeserializeObject<JArray>(content)
                        .Select(item => {
                            var doc = BsonDocument.Parse(JsonConvert.SerializeObject(item));
                            doc.Add("_id", item.Value<int>("id"));
                            return doc;
                        }).ToList();
                    this.CollData.InsertMany(models);
                    this.Logger.Log(string.Join(", ", models.Select(item => item["_id"].AsInt32)), LogAction.Success);
                    return;
                }
            }
        }
    }
}
