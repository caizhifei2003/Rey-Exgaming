﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;

namespace Rey.Exgaming.Capture.Sources.IGDB {
    public class IGDBCredentialManager : IIGDBCredentialManager {
        private Dictionary<string, IGDBCredential> Credentials { get; } = new Dictionary<string, IGDBCredential>() {
            { "8bf8fb45fae814db4e7f408c9eda2fd0", new IGDBCredential("8bf8fb45fae814db4e7f408c9eda2fd0", "https://api-2445582011268.apicast.io") },
            { "6a68b68df1abca04d21ef77300b43f54", new IGDBCredential("6a68b68df1abca04d21ef77300b43f54", "https://api-2445582011268.apicast.io") }
        };
        private int Index { get; set; } = 0;

        public void Fail(string key) {
            this.Credentials.Remove(key);
        }

        public IGDBCredential Get() {
            var keys = this.Credentials.Keys;
            if (keys.Count == 0)
                return null;

            return this.Credentials.GetValueOrDefault(keys.First());
        }
    }
}
