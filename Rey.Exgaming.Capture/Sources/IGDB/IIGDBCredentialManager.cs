﻿namespace Rey.Exgaming.Capture.Sources.IGDB {
    public interface IIGDBCredentialManager {
        IGDBCredential Get();
        void Fail(string key);
    }
}
