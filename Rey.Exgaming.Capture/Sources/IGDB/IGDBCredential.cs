﻿namespace Rey.Exgaming.Capture.Sources.IGDB {
    public class IGDBCredential {
        public string Key { get; }
        public string Url { get; }

        public IGDBCredential(string key, string url) {
            this.Key = key;
            this.Url = url;
        }
    }
}
