﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rey.Exgaming.Repository;

namespace Rey.Exgaming.Capture.Sources.IGDB {
    public class IGDBIdCapturer : Capturer {
        private IIGDBCredentialManager Credential { get; }
        private ILogger Logger { get; }
        private IMongoCollection<BsonDocument> CollId => this.Rep.Client.GetDatabase("exgaming").GetCollection<BsonDocument>("capture.igdb.id");

        public IGDBIdCapturer(IRepository rep, IIGDBCredentialManager credential, ILogger logger)
            : base(rep) {
            this.Credential = credential;
            this.Logger = logger;
        }

        public override void Start() {
            using (var client = new HttpClient()) {
                while (true) {
                    var credential = this.Credential.Get();
                    if (credential == null) {
                        throw new InvalidOperationException("cannot get credential for igdb");
                    }

                    var request = new HttpRequestMessage(HttpMethod.Get, $"{credential.Url}/games/?limit=50&scroll=1");
                    request.Headers.Add("user-key", credential.Key);
                    request.Headers.Add("Accept", "application/json");

                    var resp = client.SendAsync(request).GetAwaiter().GetResult();
                    if (resp.StatusCode == System.Net.HttpStatusCode.Forbidden) {
                        this.Logger.Log($"credential forbidden. {credential.Key}", LogAction.Skip);
                        this.Credential.Fail(credential.Key);
                        continue;
                    }

                    var scrollUrl = resp.Headers.GetValues("X-Next-Page").First();

                    while (true) {
                        request = new HttpRequestMessage(HttpMethod.Get, $"{credential.Url}{scrollUrl}");
                        request.Headers.Add("user-key", credential.Key);
                        request.Headers.Add("Accept", "application/json");

                        var content = client.SendAsync(request)
                            .GetAwaiter().GetResult()
                            .Content.ReadAsStringAsync()
                            .GetAwaiter().GetResult();

                        //! {"Err":{"status":400,"message":"Reached the end of the scroll."}}
                        var token = JsonConvert.DeserializeObject<JToken>(content);
                        var error = token as JObject;
                        if (error != null && error.Value<JToken>("Err").Value<int>("status") == 400) {
                            this.Logger.Log("all done", LogAction.Success);
                            return;
                        }

                        var list = (token as JArray).Select(item => item.Value<int>("id"))
                            .ToList();

                        var inserts = list.Where(id => this.CollId.Find(new BsonDocument("_id", id)).Count() == 0)
                            .ToList();

                        var skipped = list.Count - inserts.Count;
                        if (skipped > 0) {
                            this.Logger.Log($"{skipped}", LogAction.Skip);
                        }

                        if (inserts.Count > 0) {
                            this.CollId.InsertMany(inserts.Select(id => new BsonDocument("_id", id)));
                            this.Logger.Log(string.Join(", ", inserts), LogAction.Success);
                        }
                    }
                }
            }
        }
    }
}
