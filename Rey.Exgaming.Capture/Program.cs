﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;

namespace Rey.Exgaming.Capture {
    class Program {
        static void Main(string[] args) {
            var config = JsonConvert.DeserializeObject<JToken>(File.ReadAllText("./config.json"));
            var cfgRepo = config.Value<JToken>("repo");

            var services = new ServiceCollection();
            services.AddRepository(options => {
                options.Host = cfgRepo.Value<string>("host");
                options.Port = cfgRepo.Value<int>("port");
                options.Database = cfgRepo.Value<string>("database");
                options.User = cfgRepo.Value<string>("user");
                options.Password = cfgRepo.Value<string>("password");
                options.AuthDB = cfgRepo.Value<string>("authdb");
            });

            services.AddCapture(args);

            var provider = services.BuildServiceProvider();
            provider.GetServices<ICapturer>().ToList().ForEach(item => item.Start());
        }
    }
}
